package com.yazilimodev.service;

import com.yazilimodev.dao.CustomerDao;
import com.yazilimodev.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Cihan Y?lmaz on 9.03.2019.
 */
@Service
@Transactional
public class CustomerService {

    @Autowired
    private CustomerDao customerDao;

    public Customer save(Customer user)
    {
        return customerDao.save(user);



    }

    public void delete(int id)
    {

        customerDao.delete(id);



    }

    public void edit(Customer user)
    {
        customerDao.edit(user);


    }

    public Customer findId(int id){

        return customerDao.findId(id);

    }

    public Customer findHomeId(int id)
    {

        return customerDao.findHomeId(id);


    }


}
