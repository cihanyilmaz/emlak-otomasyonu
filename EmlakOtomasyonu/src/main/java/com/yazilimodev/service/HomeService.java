package com.yazilimodev.service;

import com.yazilimodev.dao.HomeDao;
import com.yazilimodev.model.Home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * Created by Cihan Y?lmaz on 21.12.2018.
 */

@Service
@Transactional
public class HomeService {

    @Autowired
    private HomeDao homeDao;

    public Home save(Home home)
    {
        return homeDao.save(home);



    }

    public void delete(int id)
    {

        homeDao.delete(id);



    }

    public Home findId(int id){

        return homeDao.findId(id);

    }


    public void edit(Home home)
    {
        homeDao.edit(home);


    }


    public List<Home> getDoneHomes(String HomeChoose){

        return homeDao.getDoneHomes(HomeChoose);


    }

    public List<Home> getSearchHomes(String evTercihi,String evDurumu,String evTuru,String il){


        return homeDao.getSearchHomes(evTercihi,evDurumu,evTuru,il);

    }

}
