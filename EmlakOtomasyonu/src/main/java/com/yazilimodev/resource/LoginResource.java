package com.yazilimodev.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.yazilimodev.security.CustomAuthenticationProvider;
import com.yazilimodev.service.UserService;
import com.yazilimodev.model.Login;
import com.yazilimodev.model.User;


@Component
@Path("/login")
public class LoginResource {
	
	@Autowired
	private UserService userService;
	
	
	    @GET
	    @Produces("application/json")
	    public Login loginControl() {
	        Login login = new Login();
	        //Custom providerdan o anki oturumdan kullanıcı adı çekiliyor ve dto'nun ilgili alanına set ediliyor.
	        login.setUsername(CustomAuthenticationProvider.getUsername());
	        //Eğer oturum açılmamışsa null dönebileceğinden bu durum kontrol ediliyor.
	        if (login.getUsername() != null) {//eğer oturum açılmışsa
	            User user = userService.login(login.getUsername());//Username'e göre kullanıcı nesnesi dönüyor.
	            //login nesnesinin ilgili alanlarına kullanıcı bilgileri set ediliyor.

	            login.setEnable(user.getEnable());
	            login.setRole(user.getRole());
	            login.setId(user.getId());


	        }
	        if (login.getUsername() != null)//Kullanıcı giriş yaptıysa.
	            login.setStatus(true);
	        else
	            login.setStatus(false);//Kullanıcı giriş yapmadıysa login durumunu false yapıyoruz.
	        return login;
	    }
	
	

}

