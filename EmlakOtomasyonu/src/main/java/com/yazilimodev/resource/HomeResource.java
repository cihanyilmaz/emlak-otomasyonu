package com.yazilimodev.resource;

import com.yazilimodev.model.Home;
import com.yazilimodev.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by Cihan Y?lmaz on 21.12.2018.
 */


@Component
@Path("/home")
public class HomeResource {

    @Autowired
    private HomeService homeService;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Home save(Home home)
    {

        System.out.println("gelen User::"+home.getAdres());

        return homeService.save(home);
    }

    @PUT
    @Path("/{userId}")
    @Consumes("application/json")
    @Produces("application/json")
    public void edit(@PathParam("userId") int userId,Home home)
    {
        System.out.println("put gelen:"+home.getEvDurumu());
        homeService.edit(home);



    }

    @GET
    @Path("/{userId}")
    @Produces("application/json")
    @Consumes("application/json")
    public Home findId(@PathParam("userId") int userId){

        return homeService.findId(userId);
    }


    @GET
    @Path("/search/{HomeChoose}")
    @Produces("application/json")
    public List<Home> getDoneHomes(@PathParam("HomeChoose") String HomeChoose){


        return homeService.getDoneHomes(HomeChoose);


    }

    @GET
    @Path("/search/{evTercihi}/{evDurumu}/{evTuru}/{il}")
    @Produces("application/json")
    public List<Home> getSearchHomes(@PathParam("evTercihi") String evTercihi,@PathParam("evDurumu")
            String evDurumu,@PathParam("evTuru") String evTuru,@PathParam("il") String il){


        return homeService.getSearchHomes(evTercihi,evDurumu,evTuru,il);

    }


    @DELETE
    @Path("/{userId}")
    @Produces("text/plain")
    public int delete(@PathParam("userId") int userId)
    {   homeService.delete(userId);

        return userId;

    }



}
