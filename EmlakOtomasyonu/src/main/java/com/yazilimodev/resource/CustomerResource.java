package com.yazilimodev.resource;

import com.yazilimodev.model.Customer;
import com.yazilimodev.model.Home;
import com.yazilimodev.service.CustomerService;
import com.yazilimodev.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * Created by Cihan Y?lmaz on 9.03.2019.
 */

@Component
@Path("/customer")
public class CustomerResource {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private HomeService homeService;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Customer save(Customer user)
    {

       // Home home =homeService.findId(1);
       // user.setHome(home);


        System.out.println("customerGirdi::"+user.getAdi());

        return customerService.save(user);
    }


    @DELETE
    @Path("/{userId}")
    @Produces("text/plain")
    public int delete(@PathParam("userId") int userId)
    {   customerService.delete(userId);

        return userId;

    }


    @PUT
    @Path("/{userId}")
    @Consumes("application/json")
    @Produces("application/json")
    public void edit(@PathParam("userId") int userId,Customer user)
    {
        System.out.println("putcustomerGirdi::"+user.getAdi());
        customerService.edit(user);



    }

    @GET
    @Path("/{userId}")
    @Produces("application/json")
    @Consumes("application/json")
    public Customer findId(@PathParam("userId") int userId){

        return customerService.findId(userId);
    }


    @GET
    @Path("/search/{HomeChoose}")
    @Produces("application/json")
    public Customer findHomeId(@PathParam("HomeChoose") int HomeChoose){


        return customerService.findHomeId(HomeChoose);


    }


}
