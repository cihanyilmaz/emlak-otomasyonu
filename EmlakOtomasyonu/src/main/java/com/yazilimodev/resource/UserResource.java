package com.yazilimodev.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yazilimodev.model.User;
import com.yazilimodev.service.UserService;



@Component
@Path("/user")
public class UserResource {
	
	@Autowired
	private UserService userService;
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	 public User save(User user)
	 {
		
		
		 
		 return userService.save(user);
	 }


	@DELETE
	@Path("/{userId}")
	@Produces("text/plain")
	public int delete(@PathParam("userId") int userId)
	{   userService.delete(userId);
		
		return userId;
		
	}


	@PUT
	@Path("/{userId}")
	@Consumes("application/json")
	@Produces("application/json")
	public void edit(@PathParam("userId") int userId,User user)
	{
		
		userService.edit(user);
	   
	   
		
	}

	@GET
	@Produces("application/json")
	public List<User> all()
	{
	return userService.all();	
	}

	@GET
	@Path("/{userId}")
	@Produces("application/json")
	@Consumes("application/json")
	public User findId(@PathParam("userId") int userId){
		
		return userService.findId(userId);
	}







}

