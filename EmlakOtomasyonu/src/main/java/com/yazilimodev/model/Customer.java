package com.yazilimodev.model;

import javax.persistence.*;

/**
 * Created by Cihan Y?lmaz on 6.03.2019.
 */
@Entity
@Table(name = "customer")
@NamedQuery(name = "User.Find", query = "select e from Customer e where e.home.id = ?1")
public class Customer {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;


    private String tc;
    private String adi;
    private String soyadi;
    private String telNo;
    private String mail;


    @ManyToOne
    private Home home;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTc() {
        return tc;
    }

    public void setTc(String tc) {
        this.tc = tc;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public String getSoyadi() {
        return soyadi;
    }

    public void setSoyadi(String soyadi) {
        this.soyadi = soyadi;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

   public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

}
