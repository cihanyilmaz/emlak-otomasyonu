package com.yazilimodev.model;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

//@NamedQuery(name = "User.findAll", query = "select e from User e where e.kanGrubu = ?1")
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;


	private String username;
	private String password;



	private String telephoneNumber;

	private String role;

	private String enable;



	//----------------------------------------------------------------
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}


	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}


	public String getEnable() {
		return enable;
	}
	public void setEnable(String enable) {
		this.enable = enable;
	}

	
	
	
}
