package com.yazilimodev.model;

import javax.persistence.*;

/**
 * Created by Cihan Y?lmaz on 21.12.2018.
 */
@Entity
@NamedQueries({

        @NamedQuery(name = "DoneHome.findAll", query = "select e from Home e where e.evTercihi = ?1 AND e.evDurumu='PasifEv'"),
        @NamedQuery(name = "SearchHome.findAll",
                query = "select e from Home e " +
                        "where e.evTercihi = ?1 AND e.evDurumu  = ?2 AND e.evTuru  = ?3 AND e.il  = ?4"),
})
@Table(name = "home")
public class Home {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String il;

    private String adres;

    private String evDurumu;

    private String kartNumarasi;

    private String evToplamAlan;

    private String evOdaSayisi;

    private String evTuru;

    private String yapimTarihi;

    private String evYasi;

    private String evTercihi;

    private String evFiyat;




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIl() {
        return il;
    }

    public void setIl(String il) {
        this.il = il;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getEvDurumu() {
        return evDurumu;
    }

    public void setEvDurumu(String evDurumu) {
        this.evDurumu = evDurumu;
    }

    public String getKartNumarasi() {
        return kartNumarasi;
    }

    public void setKartNumarasi(String kartNumarasi) {
        this.kartNumarasi = kartNumarasi;
    }

    public String getEvToplamAlan() {
        return evToplamAlan;
    }

    public void setEvToplamAlan(String evToplamAlan) {
        this.evToplamAlan = evToplamAlan;
    }

    public String getEvOdaSayisi() {
        return evOdaSayisi;
    }

    public void setEvOdaSayisi(String evOdaSayisi) {
        this.evOdaSayisi = evOdaSayisi;
    }

    public String getEvTuru() {
        return evTuru;
    }

    public void setEvTuru(String evTuru) {
        this.evTuru = evTuru;
    }

    public String getYapimTarihi() {
        return yapimTarihi;
    }

    public void setYapimTarihi(String yapimTarihi) {
        this.yapimTarihi = yapimTarihi;
    }

    public String getEvYasi() {
        return evYasi;
    }

    public void setEvYasi(String evYasi) {
        this.evYasi = evYasi;
    }

    public String getEvTercihi() {
        return evTercihi;
    }

    public void setEvTercihi(String evTercihi) {
        this.evTercihi = evTercihi;
    }

    public String getEvFiyat() {
        return evFiyat;
    }

    public void setEvFiyat(String evFiyat) {
        this.evFiyat = evFiyat;
    }
}
