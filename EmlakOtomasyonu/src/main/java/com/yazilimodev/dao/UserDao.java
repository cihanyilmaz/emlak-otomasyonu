package com.yazilimodev.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.yazilimodev.model.User;



@Repository
public class UserDao {
	
	//@Autowired //@Autowired
    //private SessionFactory sessionFactory;
	
	@PersistenceContext
	public EntityManager entityManager;
	
	public User save(User user)
	{
		//sessionFactory.getCurrentSession().save(user);
		entityManager.persist(user);
		return user;
	}
	
	public void delete(int id)
	{
		
		//sessionFactory.getCurrentSession().delete(findId(id));
		entityManager.remove(findId(id));
		
	   
	}

	public void edit(User user)
	{
		 //sessionFactory.getCurrentSession().update(user);
		entityManager.merge(user);
		
	
	}
	public List<User> all()
	{
		
		//Session session = this.sessionFactory.getCurrentSession();
        //List<User> musteriListe = session.createQuery("FROM User").list();
		 Criteria criteria = entityManager.unwrap(Session.class).createCriteria(User.class);
		 List results = criteria.list();
		 return results;
	}
	
	public User findId(int id){
		
		return entityManager.find(User.class, id);
				//sessionFactory.getCurrentSession().get(User.class, id);
		
	}
	
	
	
	public User login(String username){
		
		//Criteria criteria =sessionFactory.getCurrentSession().createCriteria(User.class);
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		return (User) criteria.uniqueResult();
		
		
	}








}

