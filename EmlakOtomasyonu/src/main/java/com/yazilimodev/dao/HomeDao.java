package com.yazilimodev.dao;

import com.yazilimodev.model.Home;
import com.yazilimodev.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Cihan Y?lmaz on 21.12.2018.
 */



@Repository
public class HomeDao {

    @PersistenceContext
    public EntityManager entityManager;


    public Home save(Home home)
    {
        //sessionFactory.getCurrentSession().save(user);
        entityManager.persist(home);
        return home;
    }
    public void delete(int id)
    {

        //sessionFactory.getCurrentSession().delete(findId(id));
        entityManager.remove(findId(id));


    }
    public void edit(Home home)
    {
        //sessionFactory.getCurrentSession().update(user);
        entityManager.merge(home);


    }

    public Home findId(int id){

        return entityManager.find(Home.class, id);
        //sessionFactory.getCurrentSession().get(User.class, id);

    }
    public List<Home> getDoneHomes(String HomeChoose){



        System.out.print("gelen kangrubu"+HomeChoose);
        Query query =entityManager.createNamedQuery("DoneHome.findAll");
        query.setParameter(1, HomeChoose);

        List results=query.getResultList();
        return results;



    }

    public List<Home> getSearchHomes(String evTercihi,String evDurumu,String evTuru,String il){




        Query query =entityManager.createNamedQuery("SearchHome.findAll");
        query.setParameter(1, evTercihi);
        query.setParameter(2, evDurumu);
        query.setParameter(3, evTuru);
        query.setParameter(4, il);

        List results=query.getResultList();
        return results;



    }


}
