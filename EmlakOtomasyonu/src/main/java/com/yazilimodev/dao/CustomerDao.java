package com.yazilimodev.dao;

import com.yazilimodev.model.Customer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by Cihan Y?lmaz on 9.03.2019.
 */

@Repository
public class CustomerDao {


    @PersistenceContext
    public EntityManager entityManager;

    public Customer save(Customer user)
    {
        //sessionFactory.getCurrentSession().save(user);
        entityManager.persist(user);
        return user;
    }

    public void delete(int id)
    {

        //sessionFactory.getCurrentSession().delete(findId(id));
        entityManager.remove(findId(id));


    }

    public void edit(Customer user)
    {
        //sessionFactory.getCurrentSession().update(user);
        entityManager.merge(user);


    }

    public Customer findId(int id){

        return entityManager.find(Customer.class, id);
        //sessionFactory.getCurrentSession().get(User.class, id);

    }

    public Customer findHomeId(int id)
    {
      //  Query query =entityManager.createNamedQuery("User.Find");
       // query.setParameter(1, id);

       // Customer a= (Customer) query.getSingleResult();
       // return a;


        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Customer.class);
        criteria.add(Restrictions.eq("home.id", id));
        return (Customer) criteria.uniqueResult();


    }




}
