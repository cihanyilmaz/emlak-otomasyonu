/**
 * Created by Cihan Y?lmaz on 10.03.2019.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone,Handlebars){


    var CustomerModel=Backbone.Model.extend({

        urlRoot:'/rest/customer',

        validate:function (ev) {
            debugger
            if(ev.tc.trim()==""){
                return "tcAdd";
            }

            if(ev.adi.trim()=="")
            {
                return "usernameAdd";
            }
            if(ev.soyadi=="")
            {
                return "surnameAdd";
            }
            if(ev.telNo.trim()=="")
            {
                return "telAdd";
            }
            if(ev.mail=="")
            {
                return "epostaAdd";
            }


        }



    });

    return CustomerModel;

});


