define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone,Handlebars){



    var LoginModel=Backbone.Model.extend({

        urlRoot:'/rest/login'

    });

    return LoginModel;



});

