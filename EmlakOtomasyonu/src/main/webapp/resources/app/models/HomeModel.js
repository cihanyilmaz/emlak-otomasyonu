define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone,Handlebars){


    var HomeModel=Backbone.Model.extend({

        urlRoot:'/rest/home',
        validate:function (ev) {
            debugger
            if(ev.adres.trim()==""){
               return "adresAdd";
            }

            if(ev.kartNumarasi.trim()=="")
            {
                return "kartNoAdd";
            }
            if(ev.evToplamAlan=="")
            {
                return "evAlanAdd";
            }
            if(ev.evOdaSayisi.trim()=="")
            {
                return "odaSayiAdd";
            }
            if(ev.yapimTarihi=="")
            {
                return "yapimTarihAdd";
            }
            if(ev.evYasi.trim()=="")
            {
                return "evYasAdd";
            }
            if(ev.evFiyat.trim()=="")
            {
                return "fiyatAdd";
            }

        }


    });

    return HomeModel;

});


