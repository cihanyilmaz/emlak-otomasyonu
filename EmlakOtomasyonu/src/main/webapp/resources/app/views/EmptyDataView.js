define(['jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/EmptyDataTemplate.html'], function ($, _, Backbone, Handlebars, EmptyDataTemplate) {

    var EmptyDataView = Backbone.View.extend({
        tagName: "tr",
        model: {defaults: {column: 0}},
        render: function () {
            var that = this;
            var template = Handlebars.compile(EmptyDataTemplate);
            var myHtml = template(that.model);
            that.$el.html(myHtml);
            return this;
        }
    });

    return EmptyDataView;
});
