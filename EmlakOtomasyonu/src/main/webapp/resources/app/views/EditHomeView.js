define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/HomeModel',
    'text!app/templates/EditHomeTemplate.html',





], function($, _,Backbone,Handlebars,HomeModel,EditHomeTemplate){


    var editHomeView=Backbone.View.extend({
        el:'.page',
        model:HomeModel,
        events:{


            'click #updateHome':'updateHome',



        },
        updateHome:function () {
            debugger
            var home=new HomeModel({id:$("#id").val()});

            home.set("il",$("#iller").val());
            home.set("adres",$("#adres").val());
            home.set("evDurumu",$("#evDurumu").val());
            home.set("kartNumarasi",$("#kartNo").val());
            home.set("evToplamAlan",$("#evAlan").val());
            home.set("evOdaSayisi",$("#odaSayi").val());
            home.set("evTuru",$("#evTuru").val());
            home.set("yapimTarihi",$("#yapimTarih").val());
            home.set("evYasi",$("#evYas").val());
            home.set("evTercihi",$("#evHali").val());
            home.set("evFiyat",$("#fiyat").val());




            home.save();


            window.location.href='#/searchHomes';

            return false;



        },


        render:function () {

            var that = this;
            var template = Handlebars.compile(EditHomeTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;


        }



    });



    return editHomeView;

});

