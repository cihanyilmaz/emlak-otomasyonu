/**
 * Created by Cihan Y?lmaz on 6.03.2019.
 */

define(['jquery', 'underscore', 'backbone','../models/HomeModel'], function ($, _, Backbone,HomeModel) {
    var SearchHomes = Backbone.Collection.extend({
        model: HomeModel,

        initialize: function ( options) {
            this.evTercihi = options.evTercihi;
            this.evDurumu=options.evDurumu;
            this.evTuru=options.evTuru;
            this.il=options.il;


        },
        url:function () {
            return "rest/home/search/"
                +  this.evTercihi+"/"+ this.evDurumu+"/"+this.evTuru+"/"+this.il;

        }

    });
    return SearchHomes;
});

