/**
 * Created by Cihan Y?lmaz on 16.04.2019.
 */
/**
 * Created by Cihan Y?lmaz on 6.03.2019.
 */

define(['jquery', 'underscore', 'backbone','../models/CustomerModel'], function ($, _, Backbone,CustomerModel) {
    var SearchCustomer = Backbone.Collection.extend({
        model: CustomerModel,

        initialize: function ( options) {

            this.id = options.home_id;



        },
        url:function () {
            return "rest/customer/search/"
                +  this.id;

        }

    });
    return SearchCustomer;
});

/**
 * Created by Cihan Y?lmaz on 8.03.2019.
 */

