define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/NewUserTemplate.html',
    'app/models/UserModel',

], function($, _,Backbone,Handlebars,NewUserTemplate,UserModel) {

    var NewUserView = Backbone.View.extend({

            el: '.page',
        events:{
            'click #new':'new',


        },

        new:function () {

            var user=new UserModel();

            user.set("username",$("#username").val());
           // user.set('adiSoyadi',$("#nameSurname").val());
            user.set('enable',0);
            user.set('password',$("#password").val());
            user.set('role',"tanimsiz");
           // user.set('adres',$("#adres").val());
           // user.set('telefon',$("#telefon").val());
            //user.set('kanGrubu',$("#kanGrubu").val());
           // user.set('eposta',$("#eposta").val());

            if($("#kayitKodu").val()!="123")
            {
                $(".kayitKodu").tooltip("show");

                return false;
            }


            if(!user.isValid())
            {

                $("." + user.validationError).tooltip("show");
                return false;

            }else {

                user.save();
                window.location.href='#/';
                return false;


            }






        },

            render: function () {

                this.$el.html(NewUserTemplate);
                return this;

            }


        }
    );
    return NewUserView;





});
