define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'text!app/templates/SearchBenefactorTemplate.html',
    'text!app/templates/UserListTemplate.html',
    'app/collections/UserSearchCollection',



], function($, _,Backbone,Handlebars,UserModel, SearchBenefactorTemplate,UserListTemplate,UserSearchCollection){


    var SearchBenefactorEventView=Backbone.View.extend({
        tagName:'tr',
        model:UserModel,

        render: function () {
            var that = this;
            var template = Handlebars.compile(UserListTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;
        }




    });
    var SearchBenefactorView=Backbone.View.extend({

        el: $(".page"),
        events:{
            'click #searchBenefactor': 'search',

        },

        search:function () {

        var kanGrubu=$("#kanGrubu").val();

            var search = new UserSearchCollection( {
                kanGrubu: kanGrubu,


            });

            search.fetch({

                success: function (m_travels) {


debugger

                    $(".resultRow").parent().empty();



                        m_travels.each(function (m_travel) {

                            var searchView = new SearchBenefactorEventView();
                            searchView.model = m_travel;
                            $("#travelList").append(searchView.render().el);
                        });








                }
            });

        },


        render: function () {
            this.$el.html(SearchBenefactorTemplate);
            return this;
        }
    });


    return {
        SearchBenefactorView: SearchBenefactorView,
        SearchBenefactorEventView: SearchBenefactorEventView
    };




});

