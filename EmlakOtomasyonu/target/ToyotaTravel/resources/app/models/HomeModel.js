define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone,Handlebars){


    var HomeModel=Backbone.Model.extend({

        urlRoot:'/rest/home',


    });

    return HomeModel;

});


