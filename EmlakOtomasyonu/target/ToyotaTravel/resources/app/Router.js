define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'moment',
    'spin',
    'app/views/LoginView',
    'app/models/LoginModel',
    'app/models/UserModel',
    'app/views/NewUserView',
    'app/views/PanelView',
    'app/views/SearchBenefactorView',
    'app/views/NewHomeView',
    'app/views/SearchHomeView',
    'app/models/HomeModel',
    'app/views/NewCustomerView',
    


], function($, _,Backbone,Handlebars,moment,Spinner,LoginView,LoginModel,UserModel,
            NewUserView,PanelView,SearchBenefactorView,NewHomeView,SearchHomeView,HomeModel,NewCustomerView){

    var Router = Backbone.Router.extend({

        routes: {
            '': 'home',
            'newUser':'yeniBagisci',
            'panel':'panel',
            'bagisciAra':'bagisciAra',
            'newHome':'newHome',
            'searchDoneHomes':'searchDoneHomes',
            'searchHomes':'searchHomes',
            'addCustomer/:id':'addCustomer'


           

        },
        initialize: function () {
            this.newCustomerView=new NewCustomerView();
            this.searchHomeView=new SearchHomeView.SearchHomeView();  NewCustomerView
            this.newHomeView=new NewHomeView();
            this.loginView=new LoginView();
            this.newUserView=new NewUserView();
            this.panelView=new PanelView();
            this.searchBenefactorView=new SearchBenefactorView.SearchBenefactorView();
        },
        addCustomer:function (id) {

            debugger
            var that=this;
            var homeId=id;
            var homeModel=new HomeModel({id:homeId});

            homeModel.fetch({
                success:function (m_home) {
                    //  var editUserView=new EditUserView();
                    //  editUserView.model=m_user;
                    //  editUserView.render();

                    that.newCustomerView.model=m_home;
                    that.newCustomerView.render();
                }

            });




        },

        searchHomes:function () {

            var that=this;
            that.searchHomeView.render();


        },

        searchDoneHomes:function () {




        },

        bagisciAra:function () {

            var that=this;
            this.searchBenefactorView.render();

        },
        home:function () {
    // var homeView=new HomeView();
      // homeView.render();
            var that=this;
           that.loginView.render();
        },

        yeniBagisci:function () {
            var that=this;
            this.newUserView.render();

        },
        panel:function () {
            var that=this;
            var loginUser=new LoginModel();
            loginUser.fetch({
                success:function (m_loginUser) {



                    var id=m_loginUser.toJSON().id;

                    var user=new UserModel({id:id});

                    user.fetch({
                        success:function (m_user) {


                            that.panelView.model=m_user;
                            that.panelView.render();

                        }

                    });



                }


            });



        },


        newHome:function () {

            var that=this;
            this.newHomeView.render();

        }


    });

    return Router;

});