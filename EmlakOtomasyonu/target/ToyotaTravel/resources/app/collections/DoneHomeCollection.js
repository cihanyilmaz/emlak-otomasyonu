define(['jquery', 'underscore', 'backbone','../models/HomeModel'], function ($, _, Backbone,HomeModel) {
    var DoneHomes = Backbone.Collection.extend({
        model: HomeModel,

        initialize: function ( options) {
            this.kanGrubu = options.kanGrubu;


        },
        url:function () {
            return "rest/home/search/"
                + this.kanGrubu;

        }

    });
    return DoneHomes;
});

