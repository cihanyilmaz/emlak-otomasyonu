/**
 * Created by Cihan Y?lmaz on 6.03.2019.
 */

define(['jquery', 'underscore', 'backbone','../models/HomeModel'], function ($, _, Backbone,HomeModel) {
    var SearchHomes = Backbone.Collection.extend({
        model: HomeModel,

        initialize: function ( options) {
            this.id = options.id;



        },
        url:function () {
            return "rest/home/"
                +  this.id;

        }

    });
    return SearchHomes;
});

/**
 * Created by Cihan Y?lmaz on 8.03.2019.
 */
