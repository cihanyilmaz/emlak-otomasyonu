define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/HomeModel',
    'text!app/templates/AddCustomerTemplate.html',
    'app/models/CustomerModel',




], function($, _,Backbone,Handlebars,HomeModel,AddCustomerTemplate,CustomerModel){


    var newCustomerView=Backbone.View.extend({
        el:'.page',
        model:HomeModel,
        events:{


            'click #newCustomer':'newCustomer',



        },
        newCustomer:function () {

            var homeId=$("#homeId").val();
              var tc=$("#tc").val();
        var name=$("#name").val();
        var surname=$("#surname").val();
        var tel=$("#tel").val();
        var mail=$("#mail").val();
            var customer =new CustomerModel();
            var home=new HomeModel({id:$("#homeId").val()});
        home.fetch({
            success:function () {
                debugger
                home.set("evDurumu","PasifEv");




                customer.set("tc",tc);
                customer.set("adi",name);
                customer.set("soyadi",surname);
                customer.set("telNo",tel);
                customer.set("mail",mail);
                customer.set("home",home);

                if(!customer.isValid())
                {
                    $("." + customer.validationError).tooltip("show");

                    return false;
                }else {

                    customer.save();
                    home.save();
                    window.location.href='#/searchHomes';

                    return false;

                }



            }


        });










        },


        render:function () {

            var that = this;
            var template = Handlebars.compile(AddCustomerTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;


        }



    });



    return newCustomerView;

});
