/**
 * Created by Cihan Y?lmaz on 6.03.2019.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/HomeModel',
    'text!app/templates/SearchHomeTemplate.html',
    'text!app/templates/SearchHomeListTemplate.html',
    'app/collections/SearchHomeCollection',
    'app/views/EmptyDataView'




], function($, _,Backbone,Handlebars,HomeModel, SearchHomeTemplate,SearchHomeListTemplate,SearchHomeCollection,EmptyDataView){


    var SearchHomeEventView=Backbone.View.extend({
        tagName:'tr',
        model:HomeModel,

        render: function () {
            var that = this;
            debugger
            var template = Handlebars.compile(SearchHomeListTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;
        }




    });
    var SearchHomeView=Backbone.View.extend({

        el: $(".page"),
        events:{
            'click #searchHomeBtn': 'searchHomeBtn',
            'click .deleteHome22': 'homeDelete',

        },

        homeDelete:function () {
            debugger
            console.log( $('#homeDeleted').val());
            var deleteHomeId=$('#homeDeleted').val();

            var deleteHome =new HomeModel({id:deleteHomeId});




                deleteHome.destroy();


          //  $('#homeDelete').val();


        },

        searchHomeBtn:function () {

            var evHali=$("#evHali").val();
            var evTuru=$("#evTuru").val();
            var evDurumu=$("#evDurumu").val();
            var iller=$("#iller").val();

            var search = new SearchHomeCollection( {

                evTercihi:evHali,
                evDurumu:evDurumu,
                evTuru:evTuru,
                il:iller


            });

            search.fetch({

                success: function (m_travels) {


                    debugger

                    $(".resultRow").parent().empty();

                    if (m_travels.size() == 0) {
                        var emptyDataView = new EmptyDataView();
                        emptyDataView.model = {column: 7};
                        $("#HomeList ").append(emptyDataView.render().el);

                    } else {

                        m_travels.each(function (m_travel) {

                            var searchView = new SearchHomeEventView();
                            searchView.model = m_travel;


                            //  $("#HomeList").append(searchView.render().el);  {model: contacts, users: users}
                            $("#HomeList").append(searchView.render().el);
                        });



                    }










                }
            });

        },


        render: function () {
            this.$el.html(SearchHomeTemplate);
            return this;
        }
    });


    return {
        SearchHomeView: SearchHomeView,
        SearchHomeEventView: SearchHomeEventView
    };




});


