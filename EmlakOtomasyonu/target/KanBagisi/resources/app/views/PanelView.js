define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/PanelTemplate.html',
    'app/models/UserModel',

], function($, _,Backbone,Handlebars,PanelTemplate,UserModel) {

    var PanelView = Backbone.View.extend({

            el: '.page',
        events:{
            'click #update':'update',


        },
        update:function () {

            var user =new UserModel({id:$("#id").val()});
            var newValues=new UserModel();
            newValues.set("username",$("#username").val());
            newValues.set("adiSoyadi",$("#nameSurname").val());
            newValues.set("password",$("#password").val());
            newValues.set("telefon",$("#telefon").val());
            newValues.set("eposta",$("#eposta").val());
            newValues.set("adres",$("#adres").val());
            newValues.set("kanGrubu",$("#kanGrubu").val());
            if(!newValues.isValid())
            {

                $("." + newValues.validationError).tooltip("show");
                return false;

            }else{

                user.fetch({
                    success:function (m_user) {
                        debugger
                        m_user.set("username",$("#username").val());
                        m_user.set("adiSoyadi",$("#nameSurname").val());
                        m_user.set("password",$("#password").val());
                        m_user.set("telefon",$("#telefon").val());
                        m_user.set("eposta",$("#eposta").val());
                        m_user.set("adres",$("#adres").val());
                        m_user.set("kanGrubu",$("#kanGrubu").val());

                        m_user.save();
                        window.location.href = '#/';
                        return false;
                    }

                });


            }



        },


            render: function () {

                var that = this;
                var template = Handlebars.compile(PanelTemplate);
                var myHtml = template(that.model.toJSON());
                that.$el.html(myHtml);
                return this;

            }


        }
    );
    return PanelView;





});

