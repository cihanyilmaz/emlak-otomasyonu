/**
 * Created by Cihan Y?lmaz on 6.03.2019.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/HomeModel',
    'text!app/templates/SearchDoneHomeTemplate.html',
    'text!app/templates/SearchDoneHomeListTemplate.html',
    'app/collections/SearchDoneHomeCollection',
    'app/models/CustomerModel',
    'app/collections/SearchCustomer',
    'app/views/EmptyDataView'



], function($, _,Backbone,Handlebars,HomeModel, SearchDoneHomeTemplate,SearchDoneHomeListTemplate,SearchHomeCollection,CustomerModel
,SearchCustomer,EmptyDataView){


    var SearchDoneHomeEventView=Backbone.View.extend({
        tagName:'tr',
        model:CustomerModel,

        render: function () {
            var that = this;
            debugger
            var template = Handlebars.compile(SearchDoneHomeListTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;
        }




    });
    var SearchDoneHomeView=Backbone.View.extend({

        el: $(".page"),
        events:{
            'click #searchDoneHomes': 'searchHomeBtn',

        },

        searchHomeBtn:function () {

            var evHali=$("#evHali").val();

            var search = new SearchHomeCollection( {

                evTercihi:evHali



            });

            search.fetch({

                success: function (m_travels) {




                    $(".resultRow").parent().empty();
                    if (m_travels.size() == 0) {
                        var emptyDataView = new EmptyDataView();
                        emptyDataView.model = {column: 7};
                        $("#HomeDoneList ").append(emptyDataView.render().el);
                    } else {

                        m_travels.each(function (m_travel) {





                            var searchView = new SearchDoneHomeEventView();


                            var customer =new SearchCustomer({home_id:m_travel.id});



                            customer.fetch({success:function (cust) {
                                debugger
                                var c=new CustomerModel();
                                c=cust.first();
                                searchView.model =c;

                                $("#HomeDoneList").append(searchView.render().el);


                            }});


                            //  $("#HomeList").append(searchView.render().el);  {model: contacts, users: users}


                        });








                    }


                    }


            });

        },


        render: function () {
            this.$el.html(SearchDoneHomeTemplate);
            return this;
        }
    });


    return {
        SearchDoneHomeView: SearchDoneHomeView,
        SearchDoneHomeEventView: SearchDoneHomeEventView
    };




});


