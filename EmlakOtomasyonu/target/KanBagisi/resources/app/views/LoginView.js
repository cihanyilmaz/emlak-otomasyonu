define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/LoginTemplate.html'

], function($, _,Backbone,Handlebars,LoginTemplate) {

    var LoginView = Backbone.View.extend({

            el: '.page',

            render: function () {

                this.$el.html(LoginTemplate);
                return this;

            }


        }
    );
    return LoginView;





});
