/**
 * Created by Cihan Y?lmaz on 6.03.2019.
 */

define(['jquery', 'underscore', 'backbone','../models/HomeModel'], function ($, _, Backbone,HomeModel) {
    var SearchDoneHomes = Backbone.Collection.extend({
        model: HomeModel,

        initialize: function ( options) {
            this.evTercihi = options.evTercihi;


        },
        url:function () {
            return "rest/home/search/"
                +  this.evTercihi;

        }

    });
    return SearchDoneHomes;
});


