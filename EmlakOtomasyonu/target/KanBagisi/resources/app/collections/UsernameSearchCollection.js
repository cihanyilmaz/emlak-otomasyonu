define(['jquery', 'underscore', 'backbone', '../models/TravelModel'], function ($, _, Backbone, TravelModel) {


    var UsernameSearch=Backbone.Collection.extend({

        model: TravelModel,
        initialize: function ( options) {
            this.username = options.username;


        },
        url:function () {
            return "rest/travelAdmin/searchUsername/"
                + this.username;

        }




    });

    return UsernameSearch;



});
