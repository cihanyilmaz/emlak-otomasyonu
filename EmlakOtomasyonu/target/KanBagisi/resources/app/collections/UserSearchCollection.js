define(['jquery', 'underscore', 'backbone', '../models/UserModel'], function ($, _, Backbone, UserModel) {


    var UserSearch=Backbone.Collection.extend({

        model: UserModel,
        initialize: function ( options) {
            this.kanGrubu = options.kanGrubu;


        },
        url:function () {
            return "rest/user/search/"
                + this.kanGrubu;

        }




    });

    return UserSearch;



});
